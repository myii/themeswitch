#ifndef SHELLEXEC_H
#define SHELLEXEC_H

#include <QObject>
#include <QProcess>

class Shellexec : public QObject {
    Q_OBJECT
public:
    explicit Shellexec(QObject *parent = 0);
    Q_INVOKABLE QString launch(const QString program);

private:
    QProcess *m_process;
};

#endif
