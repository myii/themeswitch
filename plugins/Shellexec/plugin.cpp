#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "shellexec.h"

void ShellexecPlugin::registerTypes(const char *uri) {

    qmlRegisterType<Shellexec>("Shellexec", 1, 0, "Shellexec");
}
