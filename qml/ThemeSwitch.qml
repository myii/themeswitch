import QtQuick 2.7
import Ubuntu.Components 1.3
import Shellexec 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'themeswitch.danfro'

    Shellexec {
      id: launcher
    }

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: 'ThemeSwitch'

            Label {
              id: versionText
              anchors {right: parent.right; rightMargin: units.gu(2); verticalCenter: parent.verticalCenter}
              font.italic: true
              font.weight: Font.Light
              fontSize: "normal"
              text: "v1.0.1"
            }
        }

        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: units.gu(4)

            Label {
                id: descriptionLabel1
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr('The system theme is going to be changed to <b>%1</b>.<br>Unity8 has to be restarted to apply the changes.').arg(getTheme())
                wrapMode: Text.WordWrap
                horizontalAlignment:  Text.AlignHCenter
            }

            Label {
                id: descriptionLabel2
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr('Save your work before proceeding!')
                color: theme.palette.normal.negative
            }

            Button{
                id: changeButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Change theme")
                color: theme.palette.normal.positive
                onClicked: switchTheme()
            }

            Button{
                text: i18n.tr("Close app")
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.foreground
                onClicked: Qt.quit()
            }
        }

        Row {
          anchors {
            bottom: parent.bottom;
            bottomMargin: units.gu(3);
            horizontalCenter: parent.horizontalCenter
          }
          Label {
              id: bugreport
              text: i18n.tr("Report an issue: ")
          }
          Label {
              id: buglink
              text: i18n.tr("ThemeSwitch on GitLab")
              color: theme.palette.normal.activity

              MouseArea {
                  anchors.fill: parent
                  onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/themeswitch/issues')
              }
          }
        }

    }

    function switchTheme() {
      var themeToSet = getTheme()
      console.log("set theme to: " + themeToSet)
      //make sure we only proceed if a valid theme is going to be set
      if (themeToSet == "SuruDark" || themeToSet == "Ambiance") {
          var cmd = '/bin/sh -c \"sed -i -e \'s:\\(theme=Ubuntu.Components.Themes.\\).*:\\1' +
            themeToSet + ':\' ~/.config/ubuntu-ui-toolkit/theme.ini\"'
          // console.log(cmd)
          launcher.launch(cmd);
          launcher.launch("restart unity8");
      }
    }

    function getTheme() {
      var themeToSet
      if (theme.name == "Ubuntu.Components.Themes.SuruDark") {
          themeToSet = "Ambiance";
      } else if (theme.name == "Ubuntu.Components.Themes.Ambiance") {
          themeToSet = "SuruDark";
      }
      return themeToSet
    }
}
