# *ThemeSwitch*

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/themeswitch.danfro)

An [UBports](https://ubports.com) app to quickly switch between Ambiance and Sur-Dark system theme.

Just start the app and tap/click on "Change theme" to restart Unity8 with the new theme.
If you need to save some work first, tap/click "Close app" and change theme later.

##### !!!Warning, this app runs unconfined and therefore may harm your system if not used properly!!!

P.S. I do know [UT Tweak Tools](https://gitlab.com/myii/ut-tweak-tool) can change system theme as well - **but** not as fast as *ThemeSwitch*! :-)
